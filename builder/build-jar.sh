#!/bin/bash

if [ "$VERSION" == "" ]; then
    echo "No version specified"
    exit 1
fi

cd inst/java
rm -rf *.jar

wget -q https://github.com/aws/aws-sdk-java/archive/$VERSION.tar.gz
tar xzf $VERSION.tar.gz
rm $VERSION.tar.gz

cd aws-sdk-java-$VERSION
DATE=`date -r pom.xml +'%Y-%m-%d %H:%M'`

if [ -d aws-java-sdk ]; then
    cd aws-java-sdk
    mvn dependency:copy-dependencies > /dev/null
    cp target/dependency/*.jar ../../
    cd ..
else
    # wget https://maven.repository.redhat.com/ga/net/sf/saxon/saxon9he/9.4.0.4/saxon9he-9.4.0.4.jar
    # mvn install:install-file -Dfile=saxon9he-9.4.0.4.jar -DgroupId=net.sf.saxon \
    #     -DartifactId=saxon9he -Dversion=9.4.0.4 -Dpackaging=jar
    mvn clean package  -DskipTests -Dmaven.test.skip=true -Dgpg.skip=true
    mvn dependency:copy-dependencies
    cp target/dependency/*.jar ../
    cp target/*.jar ../
fi

cd ..
rm -rf aws-sdk-java-$VERSION
cd ../..
echo $DATE
